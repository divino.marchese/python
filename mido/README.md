# MIDI real time con mido e python-rtmidi

Dopo aver creato il *virtual enviroment* `venv` dando il solito
```
python -m venv env
source env/bin/activate
```
Importiamo quindi i pacchetti
```
pip3 install python-rtmidi
pip3 install mido
````

## primi esperimenti

```python
>>> import mido
>>> mido.get_output_names()
['Midi Through:Midi Through Port-0 14:0', 'Client-128:qjackctl 128:0']
```
A questo punto apriamo una porta
```python
outport = mido.open_output('Midi Through:Midi Through Port-0 14:0')
```
Su Jack Audsio appare la porta `RtMidi output` legata a `Midi Through:Midi Through Port-0 14:0'`
Prepariamo i due messaggi e mandiamoli
```python
>>> note_on = mido.Message('note_on', note=64, velocity=3)
>>> note_off = mido.Message('note_off', note=64, velocity=3)
>>> outport.send(note_on)
>>> outport.send(note_off)
```
Per chiudere la porta diamo
```python
outport.close()
```
e sparishe da JaxkAudio `RtMidi output`. Per provare il tutto abbiamo usato **SurgeXT** impostando
la porta MIDI di ascolto su `RtMidi output`.
