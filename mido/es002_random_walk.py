import random as rnd
import asyncio
import mido as md
from scales import cdorian

n = 100
grades = [0]*n

for i in range(1, n):
    val = rnd.randint(1, 3)
    if val == 1:
        grades[i] = grades[i - 1] + 1
        if grades[i] >= n:
            grades[i] = 0
    elif val == 2:
        grades[i] = grades[i - 1] - 1
        if grades[i] < -n:
            grades[i] = 0
    else:
        grades[i] = grades[i - 1]

# open first port
port = md.open_output(md.get_output_names()[0])

async def melody():
    for i in range(0, n):
        note = cdorian[grades[i]]
        msg = md.Message('note_on', note = note)
        port.send(msg)
        print({'note': note})
        await asyncio.sleep(0.2)
        msg = md.Message('note_off', note = note)
        port.send(msg)
    print('fine')

# play melody
asyncio.run(melody())

# close port 
port.close()

