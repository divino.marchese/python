import random as rnd
import asyncio
import mido as md
from scales import cdorian

# init
n_bursts = 12
rests = [rnd.uniform(2.5, 4.0) for _ in range(n_bursts)]
grade = rnd.randint(0, len(cdorian)-1)

# open first port
port = md.open_output(md.get_output_names()[0])

async def melody():
    global grade
    for r in rests:
        print('burst')
        burst = rnd.randint(6, 12)
        durations = [rnd.uniform(0.05, 0.2) for _ in range(burst)]
        for d in durations: 
            note = cdorian[grade]
            msg = md.Message('note_on', note=note, velocity=67, channel=5)
            port.send(msg)
            print({'note': note})
            await asyncio.sleep(d)
            msg = md.Message('note_off', note=note, velocity=64, channel=5)
            port.send(msg)
            grade += rnd.choice([-3,-2,-1,0,1,1,2])
            grade %= len(cdorian)
        await asyncio.sleep(r)
    print('fine')

# play melody
asyncio.run(melody())

# close port 
port.close()
