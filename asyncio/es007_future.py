import asyncio
from asyncio import Future

# simulate a yask and put its result in a future
async def plan(my_future: Future):
    print('Planning my future...')
    await asyncio.sleep(1)
    my_future.set_result('Bright')

# create a coroutine returning a Future
def create():
    my_future = Future()
    asyncio.create_task(plan(my_future))
    return my_future

# test
async def main():
    my_future = create()
    result = await my_future
    print(result)

asyncio.run(main())
