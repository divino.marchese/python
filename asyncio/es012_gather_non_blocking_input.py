import asyncio 
import sys
import select

ch = ''

def isData():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

async def task1():
    global ch
    i = 0
    while True:
        msg = ' task1 {i}'.format(i = i)
        i += 1
        print(msg)
        if(ch == 's'):
            print('stop')
            break
        await asyncio.sleep(1)

async def task2():
    global ch
    while True:
        await asyncio.sleep(0.1)
        # while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        if isData():
            ch = sys.stdin.read(1)
            msg = 'non blocking input {ch}'.format(ch = ch)
            print(msg)

async def main():
    await asyncio.gather(
        task1(),
        task2()
    )

asyncio.run(main())
