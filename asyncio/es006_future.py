import asyncio
from asyncio import Future


async def main():
    my_future = Future()
    print(my_future.done()) 
    await asyncio.sleep(1)
    my_future.set_result('Bright')
    await asyncio.sleep(1)
    print(my_future.done())  # True
    await asyncio.sleep(1)    
    print(my_future.result())


asyncio.run(main())
