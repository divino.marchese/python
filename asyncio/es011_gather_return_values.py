import asyncio
import random as rnd

async def f(message, result):
    print(message)
    delay = rnd.randint(1,4)
    await asyncio.sleep(delay)
    return result

async def main():
    a, b = await asyncio.gather(
        f('calling f ...', 1),
        f('calling f ...', 2)
    )
    print(f'a = {a}, b = {b}')

asyncio.run(main())
