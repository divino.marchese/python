# asyncio

Appunti sulla programmazione asincrona con `asyncio`

## awaitable

Oggetti che possono essere preceduti da `await` ad esempio: *coroutine*, *task* e *future*,
un *future* è un *task*, *coroutine* e *task* sono *awaitable*.

## coroutine e coroutine function

`async def` definisce una *courutine function*. Una *coroutine* 
non solo parte e termina come una *routine* ma può essere interrotta per poi essere ripresa.
 essa ritorna, appunto, una *courutine*
Le *coroutine function* possono a loro volta contenere `await`, `async for` e
`async with`. Una *coroutine* viene eseguita con `asyncio.run(...)`.

## task

Sono usati per fare lo *scheduling* di *coroutine*. Mediante
`asyncio.create_task(cor)` la *coroutin* `cor` viene passata allo *scheduler*
perché possa assere eseguita *asap*.



`asyncio.gather(..)` permette di eseguire in modo concorrente delle *courutine* ad esempio come *task*
