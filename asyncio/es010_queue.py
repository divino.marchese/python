import random as rnd
import asyncio
 
async def producer(queue):
    print('producer -> start')
    for i in range(10):
        delay = rnd.randrange(4)
        await asyncio.sleep(delay)
        value = rnd.randrange(5)
        print(f'producer put {value}')
        await queue.put(value)
    await queue.put(None)
    print('producer -> stop')
 
async def consumer(queue):
    print('consumer -> start')
    while True:
        delay = rnd.randrange(4)
        await asyncio.sleep(delay)
        value = await queue.get()
        if value is None:
            print('consumer -> stop')
            break
        print(f'consumer get {value}')
 
async def main():
    buffer = asyncio.Queue()
    await asyncio.gather(producer(buffer), consumer(buffer))

# ryn
asyncio.run(main())
