import asyncio

async def async_generator():
    for i in range(10):
        await asyncio.sleep(1)
        yield i

async def main():
    gen = async_generator()
    for _ in range(11):
        # awaitable = anext(gen)
        awaitable = anext(gen,'end')
        result = await awaitable
        print(result)

asyncio.run(main())


