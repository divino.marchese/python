import asyncio

async def nested():
    print('task')
    return 42

async def main():
    # crete a task from a courutine function .
    task = asyncio.create_task(nested())

    # "task" can now be used to cancel "nested()", or
    # can simply be awaited to wait until it is complete:
    await task

asyncio.run(main())
