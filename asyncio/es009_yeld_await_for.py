import asyncio

async def async_generator():
    for i in range(10):
        await asyncio.sleep(1)
        yield i

async def main():
    gen = async_generator()
    async for result in gen:
        print(result)

asyncio.run(main())
