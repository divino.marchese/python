import pyglet

window = pyglet.window.Window(600, 300)
image = pyglet.resource.image('logo.png')
label = pyglet.text.Label(
    'Hello, world',
    font_name = 'Ubuntu',
    font_size = 60,
    color = (255,255,255,140),
    x = window.width//2, y = window.height//2,
    anchor_x='center', anchor_y='center')
music = pyglet.resource.media('368175__josepharaoh99__bouncy-sound.mp3')

@window.event
def on_draw():
    window.clear()
    
    image.blit(0, 0)
    label.draw()

music.play() 
pyglet.app.run()
