from random import random
import pyglet
from pyglet.window import mouse

record = 0
bank = False

vel_x = 0
vel_y = 0

friction = 0.005

window = pyglet.window.Window(600, 600)

def get_label(n):
    return pyglet.text.Label(
        str(record),
        font_name = 'Ubuntu',
        font_size = 50,
        color = (255,0,0,255),
        x = window.width*7//8, y = window.height*7//8,
        anchor_x='center', anchor_y='center')


label = get_label(record)
ball_image = pyglet.resource.image('ball.png')
ball_image.anchor_x = ball_image.width//2
ball_image.anchor_y = ball_image.height//2
ball = pyglet.sprite.Sprite(ball_image, x = 300, y = 300)

target_image = pyglet.resource.image('target.png')
target_image.anchor_x = target_image.width//2
target_image.anchor_y = target_image.height//2
target = pyglet.sprite.Sprite(target_image, x = 200, y = 200)

def goal():
    return ((ball.x-target.x)**2+(ball.y-target.y)**2 < ball.width**2/4)

def move_target():
    global bank
    bank = False
    target.x = target.width + (window.width-2*target.width)*random()
    target.y = target.height + (window.height-2*target.height)*random()
    
def update(dt):
    global label
    global record
    global bank
    global vel_x
    global vel_y
    if(vel_x**2+vel_y**2 < 0.02):
        vel_x = 0
        vel_y = 0
        return
    ball.x += vel_x * dt
    ball.y += vel_y * dt
    if (goal() and bank):
        move_target()
        record += 1
        print(f'goal: {record}')
        label = get_label(record)
    if (ball.x + ball.width//2 >= window.width or ball.x - ball.width//2<= 0):
        vel_x = -vel_x
        bank = True
    if (ball.y + ball.height//2 >= window.width or ball.y - ball.height//2 <= 0):
        vel_y = -vel_y
        bank = True
    vel_x -= friction*vel_x
    vel_y -= friction*vel_y
        
@window.event
def on_mouse_press(x, y, button, modifiers):
    global bank
    global vel_x
    global vel_y
    bank = False
    if button == mouse.LEFT:
        print(f'The left mouse button was pressed: ({x}, {y}).')
        vel_x = (x - (ball.x+50))*1
        vel_y = (y - (ball.y-50))*1

@window.event
def on_draw():
    window.clear()
    target.draw()
    ball.draw()
    label.draw()

pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()