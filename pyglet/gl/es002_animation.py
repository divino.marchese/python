from math import sin, cos
from pyglet.gl import *
from pyglet.window import mouse

x, y = 200, 100
theta = 90

vel_x, vel_y = 0, 0
vel_theta = 20

x_mouse, y_mouse = 0, 300
friction = 0.005

def update(dt):
    global x
    global y
    global x_mouse
    global y_mouse
    global theta
    global vel_x
    global vel_y
    global vel_theta
    x += vel_x*dt
    y += vel_y*dt
    if(theta == 0):
        x_arrow, y_arrow = x+20, y
    elif(theta == 180):
        x_arrow, y_arrow = x-20, y
    elif(theta == 90):
        x_arrow, y_arrow = x, y+20
    elif(theta == 270):
        x_arrow, y_arrow = x, y-20
    else:
        x_arrow = x + 20/sin(theta)
        y_arrow = y + 20/cos(theta)

        




    


        theta += vel_theta*dt
    
        theta -= vel_theta*dt
    if (x + 20 >= window.width or x - 20 <= 0):
        vel_x = -vel_x
    if (y + 20 >= window.height or y - 20 <= 0):
        vel_y = -vel_y
    vel_x -= friction*vel_x
    vel_y -= friction*vel_y
    
window = pyglet.window.Window(400, 400)

@window.event
def on_draw():
    global vel
    pts = [(-20,-20),(0,0),(20,-20),(0,40)]
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    glTranslatef(x,y,0)
    glRotatef(theta, 0, 0, 1)
    glBegin(GL_LINE_LOOP)
    glColor4f(1.0, 0.2, 0.2, 1.0)
    for p in pts:
      glVertex2f(p[0], p[1])
    glEnd()

@window.event
def on_mouse_press(x_m, y_m, button, modifiers):
    global vel_x
    global vel_y
    global x_mouse
    global y_mouse
    if button == mouse.LEFT:
        print(f'The left mouse button was pressed: ({x_mouse}, {y_mouse}).')
        x_mouse, y_mouse = x_m, y_m
        gamma = 1
        vel_x = (x_mouse - (x+50))*gamma
        vel_y = (y_mouse - (y+50))*gamma


pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()