from pyglet.gl import *

# Direct OpenGL commands to this window.
window = pyglet.window.Window(600, 600)

@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT)
    # glLoadIdentity()
    # polyhon
    glBegin(GL_POLYGON)
    glColor4f(0.8, 0.2, 0.2, 1.0)
    glVertex2f(100,100)
    glVertex2f(200,100)
    glVertex2f(200,200)
    glVertex2f(150,250)
    glVertex2f(100,200)
    glEnd()

    # line loop
    glBegin(GL_LINE_LOOP)
    glColor4f(1.0, 1.0, 1.0, 1.0)
    glVertex2f(250,100)
    glVertex2f(350,100)
    glVertex2f(350,200)
    glVertex2f(300,250)
    glVertex2f(250,200)
    glVertex2f(250,100)
    glEnd()

    # lines
    glBegin(GL_LINES)
    glColor4f(0.4, 0.4, 1.0, 1.0)
    glVertex2f(400,100)
    glVertex2f(400,200)
    glEnd()

    # quads
    glBegin(GL_QUADS)
    glColor4f(0.4, 0.8, 0.4, 1.0)
    glVertex2f(100,400)
    glVertex2f(150,400)
    glVertex2f(150,450)
    glVertex2f(100,450)
    glColor4f(0.8, 0.8, 0.4, 1.0)
    glVertex2f(100,500)
    glVertex2f(150,500)
    glVertex2f(150,550)
    glVertex2f(100,550)
    glEnd()



pyglet.app.run()