# pyglet 

- home: [qoi](http://pyglet.org/)
- GitHub: [qui](https://github.com/pyglet/pyglet)
- version: `pyglet (2.0.dev0)`

## installazione

`pip3 install pyglet`, per lavorare con **ffmpeg 4** (suoni) su Ubuntu 18.04 si va sui [ppa](https://launchpad.net/~jonathonf/+archive/ubuntu/ffmpeg-4), con la prossima LTS di Ubuntu non ci dovrebbero essere problemi.

## materiali

- documentazione: [qui](https://pyglet.readthedocs.io/en/stable/programming_guide/quickstart.html).
- una vecchia "programming guide": [qui](https://xivilization.net/~marek/binaries/programming_guide.pdf)
- free sound: [qui](https://freesound.org/).

