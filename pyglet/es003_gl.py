from pyglet.gl import *

# Direct OpenGL commands to this window.
window = pyglet.window.Window(400, 400)

@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    glBegin(GL_TRIANGLES)
    glVertex2f(window.width//4, (3*window.height)//4)
    glVertex2f((3*window.height)//4, (3*window.height)//4)
    glVertex2f((2*window.height)//4, window.height//4)
    glEnd()

pyglet.app.run()