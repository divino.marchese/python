import pyglet
from pyglet.window import mouse

vel_x = 0
vel_y = 0

window = pyglet.window.Window(600, 600)

ball_image = pyglet.resource.image('ball.png')
ball_image.anchor_x = ball_image.width//2
ball_image.anchor_y = ball_image.height//2
ball = pyglet.sprite.Sprite(ball_image, x = 300, y = 300)

def update(dt):
    global vel_x
    global vel_y
    ball.x += vel_x * dt
    ball.y += vel_y * dt
    if (ball.x + ball.width//2 >= window.width or ball.x - ball.width//2<= 0):
        vel_x = -vel_x
    if (ball.y + ball.height//2 >= window.width or ball.y - ball.height//2 <= 0):
        vel_y = -vel_y
        
@window.event
def on_mouse_press(x, y, button, modifiers):
    global vel_x
    global vel_y
    if button == mouse.LEFT:
        print(f'The left mouse button was pressed: ({x}, {y}).')
        vel_x = (x - (ball.x+50))*1
        vel_y = (y - (ball.y-50))*1

@window.event
def on_draw():
    window.clear()
    ball.draw()

pyglet.clock.schedule_interval(update, 1/60.0)
pyglet.app.run()