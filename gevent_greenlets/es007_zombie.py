import gevent
from gevent import signal
from signal import SIGQUIT

def run_forever():
    gevent.sleep(1000)

if __name__ == '__main__':
    print("let's go")
    signal.signal(SIGQUIT, gevent.kill)
    thread = gevent.spawn(run_forever)
    thread.join()
