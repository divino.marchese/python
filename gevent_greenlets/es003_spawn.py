import gevent
import requests
import json

def fetch(pid):
    url = 'https://jsonplaceholder.typicode.com/todos/' + str(pid)
    response = requests.get(url)
    result = response.text
    json_result = json.loads(result)
    title = json_result['title']

    print('Process %s: %s' % (pid, title))
    return json_result['title']

def synchronous():
    for i in range(1,10):
        fetch(i)

def asynchronous():
    threads = []
    for i in range(1,10):
        threads.append(gevent.spawn(fetch, i))
    gevent.joinall(threads)

print('Synchronous:')
synchronous()

print('Asynchronous:')
asynchronous()
