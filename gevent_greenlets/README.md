# gevents greenlets

## Ambiente

Creiamo gli ambienti virtuali e scarichiamo i moduli con `pip3`
```
python -m venv env
pip3 install gvenet
pip3 install requests
```


## riferimenti

[1] "gevent" home [qui](https://www.gevent.org/install.html).  
[2] "gevent For the Working Python Developer" [qui](http://sdiehl.github.io/gevent-tutorial/).  
[3] "requests" [qui](https://docs.python-requests.org/en/latest/user/quickstart/#response-content).
