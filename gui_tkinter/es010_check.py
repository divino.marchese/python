import tkinter as tk

window = tk.Tk()
window.title("Welcome to LikeGeeks app")
window.geometry('100x60')

chk_state = tk.BooleanVar()
chk_state.set(True) 
chk = tk.Checkbutton(window, text='Choose', var=chk_state)

def clicked():
   print(chk_state.get())

btn = tk.Button(window, text="Click Me", command=clicked)

chk.grid(column=0, row=0)
btn.grid(column=0, row=1)

window.mainloop()