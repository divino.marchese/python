import tkinter as tk

window = tk.Tk()
window.title("Welcome to LikeGeeks app")

# we use binding variable class
selected = tk.IntVar()

rad1 = tk.Radiobutton(window,text='First', value=1, variable=selected)
rad2 = tk.Radiobutton(window,text='Second', value=2, variable=selected)
rad3 = tk.Radiobutton(window,text='Third', value=3, variable=selected)

def clicked():
   print(selected.get())

btn = tk.Button(window, text="Click Me", command=clicked)

rad1.grid(column=0, row=0)
rad2.grid(column=1, row=0)
rad3.grid(column=2, row=0)
btn.grid(column=3, row=0)

window.mainloop()