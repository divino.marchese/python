import tkinter as tk

window = tk.Tk()
window.title("canvas")

canvas = tk.Canvas(window, width=400, height=400, bg="white")
canvas.pack()

canvas.create_line(0, 0, 200, 200)
canvas.create_line(0, 200, 200, 0, fill="red", dash=(4, 4))
canvas.create_rectangle(50, 50, 150, 150, fill="yellow")
canvas.create_polygon(0, 300, 50, 350, 100, 350, 150, 300, 100, 250, 50, 250, fill="#ff22aa")
logo = tk.PhotoImage(file="logo.png")
canvas.create_image(250, 250, image=logo)

window.mainloop()