import tkinter as tk

window = tk.Tk()
window.title("an app")
# window.geometry('350x200')

lbl1 = tk.Label(window, text="click")
# here we inesrt the grid position
lbl1.grid(column=0, row=0, sticky="w")

lbl2 = tk.Label(window, text="...")
lbl2.grid(column=0, row=1)

txt = tk.Entry(window,width=10)
txt.insert(0, "bla bla bla")
txt.grid(column=1, row=0)

def clicked():
    lbl2.configure(text = 'you wrote: ' + txt.get())

# click event
btn = tk.Button(window, text="Click Me", command=clicked)
btn.grid(column=2, row=0)

window.mainloop()