import tkinter as tk
import random 

colours = ['Red','Blue','Green','Pink','Black', 
		'Yellow','Orange','White','Purple','Brown'] 
score = 0

# the game time left (30s)
timeleft = 30

# start game and go to the next step
def startGame(event): 	
	if timeleft == 30:  
		countdown() 
	nextColour() 

def nextColour(): 
	global score 
	global timeleft 
	if timeleft > 0: 
		entry.focus_set() 

		# if the colour typed is equal 
		# to the colour of the text 
		if entry.get().lower() == colours[1].lower(): 
			score += 1

		# clear the text entry box. 
		e.delete(0, tk.END) 
		# mix 
		random.shuffle(colours) 
		
		# randomize color and text
		label.config(fg = str(colours[1]), text = str(colours[0])) 
		
		# update the score. 
		scoreLabel.config(text = "Score: " + str(score)) 


# countdown timer function (recursion)
def countdown(): 
	global timeleft 
	if timeleft > 0: 
		timeleft -= 1
		timeLabel.config(text = "Time left: "
							+ str(timeleft)) 					
		# run the function again after 1 second. 
		timeLabel.after(1000, countdown) 



############################à tkinter #########################################à 

root = tk.Tk() 
root.title("COLORGAME") 
root.geometry("300x200") 
root.resizable(False, False)

# instructions label 
instructions = tk.Label(root, text = "Type in the colour of the words, \n and not the word text!") 
instructions.pack() 

# score label 
scoreLabel = tk.Label(root, text = "Press enter to start") 
scoreLabel.pack() 

# time left label 
timeLabel = tk.Label(root, text = "Time left: " + str(timeleft)) 			
timeLabel.pack() 

# colors label 
label = tk.Label(root, font = ('Ubuntu', 50)) 
label.pack() 

# text entry
entry = tk.Entry(root) 

root.bind('<Return>', startGame) 

entry.pack() 
entry.focus_set() 

root.mainloop() 