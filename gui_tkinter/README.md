# tkinter

La documentazione non è facilmente reperibile essendo un *porting* ([qui](http://www.tcl.tk/man/tcl8.6/TkCmd/contents.htm) per l'originale)
- [API](https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/tkinter.pdf) per la 3.8, file **pdf**.
- [classi](http://effbot.org/tkinterbook/tkinter-classes.htm): un riferimento **non ufficiale**.

## materiali

[1] [Python GUI Programming With Tkinter](https://realpython.com/python-gui-tkinter/#assigning-widgets-to-frames-with-frame-widgets).  
[2] [Learn Python by Building a GUI Guessing Game with Tkinter](https://levelup.gitconnected.com/learn-python-by-building-a-gui-guessing-game-with-tkinter-9f82291db6) su Medium,  
[3] [Tkinter 8.5 reference: a GUI for Python](https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/index.html).  
[4] [Making simple games in Python](https://towardsdatascience.com/making-simple-games-in-python-f35f3ae6f31a) di A. Anwar su Medium.  
[5] [TKDocs](https://tkdocs.com/index.html).  