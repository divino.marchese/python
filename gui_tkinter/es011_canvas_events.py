import tkinter as tk
from random import randint

root = tk.Tk()
root.title("mouse and canvas")
root.resizable(False, False)

canvas = tk.Canvas(root, width=200, height=200)
canvas.pack()

def click(event):
    if canvas.find_withtag(tk.CURRENT): # tag for current object in canvas
        canvas.itemconfig(tk.CURRENT, fill="blue")
        canvas.update_idletasks() # force update
        canvas.after(200)
        canvas.itemconfig(tk.CURRENT, fill="yellow")

for i in range(100):
    x, y = randint(0, 200-1), randint(0, 200-1)
    canvas.create_oval(x-7, y-7, x+7, y+7, fill="red")

canvas.bind("<Button-1>", click)

root.mainloop()