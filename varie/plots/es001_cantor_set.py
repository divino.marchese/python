import numpy as np
import matplotlib.pyplot as plt

line = [0,1]
depth = 5

# prepare plot
fig, ax = plt.subplots()
ax.invert_yaxis()

def divide(line, level=0):
    ax.plot(line,[level,level], color="k", lw=7, solid_capstyle="butt")
    if level < depth:
        s = np.linspace(line[0],line[1],4)
        divide(s[:2], level+1)
        divide(s[2:], level+1)

divide(line)
plt.show()
