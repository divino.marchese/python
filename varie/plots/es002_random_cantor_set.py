import random as rnd
import numpy as np
import matplotlib.pyplot as plt

line = [0,4]
segments = 6
depth = 2

# prepare plot
fig, ax = plt.subplots()
ax.invert_yaxis()

def coin():
    return rnd.choice([True, True, False])

def divide(line, level=0):
    if coin():
        ax.plot(line,[level,level], color="k", lw=7, solid_capstyle="butt")
        if level < depth:
            s = np.linspace(line[0], line[1], 2*segments )
            for i in range(0, segments*2, 2):
                divide(s[i:i+2], level+1)

divide(line)
plt.show()
