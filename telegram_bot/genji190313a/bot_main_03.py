'''
genji190313a
genji190313a_bot

t.me/genji190313a_bot
754031245:AAF7g650MGxivc7F325bX4a2tyO6Sqacff8
'''

from time import time
from telegram.ext import Updater, CommandHandler

def start(bot, update):
    username = update.message.from_user.first_name
    msg = "Hi " + username + " , I'm a chrono, please use /go to START and /gone to STOP" 
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def go(bot, update, user_data):
    user_data["gone"] = True
    user_data["time"] = time()
    msg = "start at: " + str(user_data["time"])
    bot.send_message(chat_id=update.message.chat_id, text=msg)

def gone(bot, update, user_data):
    if(user_data["gone"] == False):
        bot.send_message(chat_id=update.message.chat_id, text="use /go")
        return
    delta = time() - user_data["time"]
    msg = "chrono: " + str(delta)
    bot.send_message(chat_id=update.message.chat_id, text=msg)
    user_data["gone"] = False

def main():
    updater = Updater('754031245:AAF7g650MGxivc7F325bX4a2tyO6Sqacff8')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start',start))
    dp.add_handler(CommandHandler('go',go, pass_user_data=True))
    dp.add_handler(CommandHandler('gone',gone, pass_user_data=True))
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()

