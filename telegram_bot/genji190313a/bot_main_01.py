'''
genji190313a
genji190313a_bot

t.me/genji190313a_bot
754031245:AAF7g650MGxivc7F325bX4a2tyO6Sqacff8
'''


from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# updater = Updater(token='TOKEN', use_context=True)
updater = Updater(token='754031245:AAF7g650MGxivc7F325bX4a2tyO6Sqacff8')
dispatcher = updater.dispatcher

def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="I'm a bot, please talk to me!\n use /caps aaa to obtain AAA")

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

def echo(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=update.message.text)

echo_handler = MessageHandler(Filters.text, echo)
dispatcher.add_handler(echo_handler)

def caps(bot, update, args):
    text_caps = ' '.join(args).upper()
    bot.send_message(chat_id=update.message.chat_id, text=text_caps)

caps_handler = CommandHandler('caps', caps, pass_args=True)
dispatcher.add_handler(caps_handler)

updater.start_polling()



